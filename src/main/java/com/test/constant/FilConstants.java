package com.test.constant;

import java.math.BigDecimal;

public class FilConstants {

    /**
     * 节点类型-私有节点
     */
    public static final String PRIVATE_NODE = "private";

    /**
     * infura节点
     */
    public static final String INFURA_NODE = "infura";

    /**
     *
     */
    public static final int MajUnsignedInt  = 0;
    /**
     *
     */
    public static final int MajNegativeInt  = 1;
    /**
     *
     */
    public static final int MajByteString  = 2;
    /**
     *
     */
    public static final int MajTextString  = 3;
    /**
     *
     */
    public static final int MajArray  = 4;
    /**
     *
     */
    public static final int MajMap  = 5;
    /**
     *
     */
    public static final int MajTag  = 6;
    /**
     *
     */
    public static final int MajOther  = 7;

    //FIL的单位，10的18次幂
    public static final BigDecimal FIL_UNIT = BigDecimal.TEN.pow(18);
    //生成新令牌
    public static final String AUTH_NEW = "Filecoin.AuthNew";
    //读取区块
    public static final String GET_BLOCK = "Filecoin.ChainGetBlock";
    //读取区块内消息
    public static final String GET_BLOCK_MESSAGES = "Filecoin.ChainGetBlockMessages";
    //获取当前链头
    public static final String GET_CHAIN_HEAD = "Filecoin.ChainHead";
    //读取指定CID的消息
    public static final String GET_CHAIN_MESSAGE = "Filecoin.ChainGetMessage";
    //估算gas费封顶值
    public static final String GET_ESTIMATE_GAS_FEE_CAP = "Filecoin.GasEstimateFeeCap";
    //估算gas费上限
    public static final String GET_ESTIMATE_GAS_FEE_LIMIT = "Filecoin.GasEstimateGasLimit";
    //估算gas用量
    public static final String GAS_ESTIMATE_GAS_PREMIUM = "Filecoin.GasEstimateGasPremium";
    //估算要在10个区块内确认消息所需gas值
    public static final String GAS_ESTIMATE_MESSAGE_GAS = "Filecoin.GasEstimateMessageGas";
    //签名消息批量推入内存池
    public static final String MPOOL_BATCH_PUSH = "Filecoin.MpoolBatchPush";
    //将一个签名消息推入内存池 (广播)
    public static final String MPOOL_PUSH = "Filecoin.MpoolPush";
    //将指定未签名消息推入内存池
    public static final String MPOOL_PUSH_MESSAGE = "Filecoin.MpoolPushMessage";
    //查询指定矿工的有效余额
    public static final String STATE_MINER_AVAILABLE_BALANCE = "Filecoin.StateMinerAvailableBalance";
    //查询读取状态
    public static final String STATE_READ_STATE="Filecoin.StateReadState";
    //查询钱包余额
    public static final String WALLET_BALANCE="Filecoin.WalletBalance";
    //将指定私钥导入钱包
    public static final String WALLET_IMPORT="Filecoin.WalletImport";
    //返回钱包中的全部抵制
    public static final String WALLET_LIST="Filecoin.WalletList";
    //使用指定地址签名数据
    public static final String WALLET_SIGN="Filecoin.WalletSign";
    //获得新的钱包地址
    public static final String WELLET_NEW="Filecoin.WalletNew";
    //使用指定地址签名消息
    public static final String WALLET_SIGN_MESSAGE="Filecoin.WalletSignMessage";
    //查询指定高度的链头集合
    public static final String CHAIN_GET_TIP_SET_BY_HEIGHT="Filecoin.ChainGetTipSetByHeight";
    //获得指定地址的下一个随机值
    public static final String GET_NONCE="Filecoin.MpoolGetNonce";
    //校验钱包地址
    public static final String WALLET_VALIDATE_ADDRESS="Filecoin.WalletValidateAddress";
    //返回指定区块消息
    public static final String CHAIN_GET_BLOCK_MESSAGE="Filecoin.ChainGetBlockMessages";


    /**
     * fil单位
     */
    public static enum Unit {
        DEFAULT("default", 0),
        ETHER("fil", 18),
        ;

        private String name;
        private BigDecimal unitFactor;

        private Unit(String name, int factor) {
            this.name = name;
            this.unitFactor = BigDecimal.TEN.pow(factor);
        }

        public BigDecimal getWeiFactor() {
            return this.unitFactor;
        }

        @Override
        public String toString() {
            return this.name;
        }

        public static Unit fromString(String name) {
            if (name != null) {
                for (Unit value : values()) {
                    if (value.name.equalsIgnoreCase(name)){
                        return value;
                    }
                }
            }
            return valueOf(name);
        }
    }
}
