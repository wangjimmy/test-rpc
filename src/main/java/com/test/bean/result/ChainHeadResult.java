package com.test.bean.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChainHeadResult implements Serializable {
    private static final long serialVersionUID = 325342751033079476L;
    private List<Map<String, String>> Cids = new ArrayList<>();
    private List<Map<String, String>> Blocks = new ArrayList<>();
    private BigInteger Height;

}
