package com.test.bean.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CommonResult implements Serializable {

    private static final long serialVersionUID = 8195296215259588461L;
    private Long id;
    private String jsonrpc;
    private String result;
}
