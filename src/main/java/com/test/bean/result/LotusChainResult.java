package com.test.bean.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class LotusChainResult implements Serializable {
    private static final long serialVersionUID = 6822954706761507318L;
    private Long id;
    private String jsonrpc;
    private ChainHeadResult result;
}
