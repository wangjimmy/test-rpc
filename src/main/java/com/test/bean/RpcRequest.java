package com.test.bean;

import com.alibaba.fastjson.JSONObject;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RpcRequest implements Serializable {

    private static final long serialVersionUID = 1251341948490475157L;
    private Integer id;
    private String method;
    private List<JSONObject> params;
    @Builder.Default
    private String jsonrpc="2.0";
}
