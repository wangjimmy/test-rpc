package com.test.bean;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
public class ListObjectRequest implements Serializable {

    private static final long serialVersionUID = 2115691957057012400L;
    private Integer id;
    private String method;
    private List<Object> params;
    @Builder.Default
    private String jsonrpc="2.0";

    public ListObjectRequest() {
    }
}
