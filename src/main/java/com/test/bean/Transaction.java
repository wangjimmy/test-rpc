package com.test.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author jimmy wang
 * 交易对象
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transaction implements Serializable {
    private static final long serialVersionUID = -9078731623634333478L;
    private String to;
    private String from;
    private Integer version;
    private Long nonce;
    private String value;
    private Long gasLimit;
    private String gasFeeCap;
    private String gasPremium;
    private Long method;
    private String params;
    private String CID;
}
