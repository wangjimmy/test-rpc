package com.test.bean;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
public class CommonRequest implements Serializable {
    private static final long serialVersionUID = -5976992669534242527L;
    private Integer id;
    private String method;
    private String[] params;
    @Builder.Default
    private String jsonrpc="2.0";

    public CommonRequest() {
    }
}
