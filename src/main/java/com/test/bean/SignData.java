package com.test.bean;

import co.nstant.in.cbor.model.ByteString;
import co.nstant.in.cbor.model.UnsignedInteger;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * @author jimmy wang
 * 签名对象
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignData implements Serializable {
    private static final long serialVersionUID = -8487086473321404727L;

    private ByteString from;

    private UnsignedInteger version;

    private ByteString to;

    private UnsignedInteger nonce;

    private ByteString value;

    private ByteString gasFeeCap;

    private ByteString gasPremium;

    private UnsignedInteger gasLimit;

    private UnsignedInteger methodNum;

    private ByteString params; //空数组
}
