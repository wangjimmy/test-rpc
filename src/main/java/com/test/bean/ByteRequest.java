package com.test.bean;

import com.google.protobuf.ByteString;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@ToString
public class ByteRequest implements Serializable {
    private static final long serialVersionUID = 5883394046714684183L;
    private Integer id;
    private String method;
    private ByteString params;
    @Builder.Default
    private String jsonrpc="2.0";

    public ByteRequest() {
    }
}
