package com.test.controller;

import com.dtflys.forest.annotation.Get;
import com.test.bean.result.CommonResult;
import com.test.bean.result.LotusChainResult;
import com.test.exception.AddressException;
import com.test.service.FileCoinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.BigInteger;

@RestController
@RequestMapping("/hello")
public class HelloController {
    @Value("${lotus.address}")
    private String address;
    @Autowired
    FileCoinService fileCoinService;

    @GetMapping("/getWalletBalance")
    public BigDecimal getWalletBalance() {
        BigDecimal result = fileCoinService.getWalletBalance();
        return result;
    }

    /**
     * 当前链头
     *
     * @return
     */
    @GetMapping("/getChainHead")
    public String getChainHead() {
        LotusChainResult result = fileCoinService.getChainHead();
        return result.getResult().toString();
    }

    @GetMapping("/getNonce")
    public BigInteger getNonce() {
        BigInteger result = fileCoinService.getNonce();
        return result;
    }

    /**
     * 校验钱包地址
     *
     * @return
     */
    @GetMapping("/checkAddress")
    public CommonResult checkAddress() {
        CommonResult result = fileCoinService.checkAddress();
        return result;
    }

    /**
     * 获得当前区块的cid
     *
     * @return
     */
    @GetMapping("/getCid")
    public String cid() {
        String blockCid = fileCoinService.getBlockCid();
        return blockCid;
    }


    @GetMapping("/getGasEstimateFeeCap")
    public String getGasEstimateFeeCap() {
        String message = fileCoinService.getGasEstimateFeeCap(getNonce(), "1");
        String gasEstimateFeeCap = fileCoinService.getGasEstimateFeeCap(message);
        return gasEstimateFeeCap;
    }

    @GetMapping("/getGasEstimateGasLimit")
    public String getGasEstimateGasLimit() {
        String result = fileCoinService.getGasEstimateGasLimit(fileCoinService.getGasEstimateFeeCap(getNonce(), "1"), cid());
        return result;
    }

    @GetMapping("walletSign")
    public String walletSign() {
        return fileCoinService.walletSign();
    }

    @GetMapping("/authNew")
    public CommonResult authNew() {
        return fileCoinService.newAuth();
    }

    @GetMapping("/mpoolPushMessage")
    public String mpoolPushMessage() throws AddressException {
        return fileCoinService.mpoolPushMessage();
    }
}
