package com.test.forest;

import com.dtflys.forest.annotation.JSONBody;
import com.dtflys.forest.annotation.Post;
import com.test.bean.CommonRequest;
import com.test.bean.ListObjectRequest;
import com.test.bean.RpcRequest;
import com.test.bean.result.CommonResult;
import com.test.bean.result.LotusChainResult;

public interface RpcClient {
    @Post(url = "http://127.0.0.1:1234/rpc/v0",
    headers = {
            "Authorization:Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBbGxvdyI6WyJyZWFkIiwid3JpdGUiLCJzaWduIiwiYWRtaW4iXX0.zTmc2QdGWlgEoSyV03sOd2NjDYqzPwiSSxzfYirmvi4"
    })
    CommonResult newAuth(@JSONBody String request);

    @Post(url = "http://127.0.0.1:1234/rpc/v0")
    String walletBalance(@JSONBody CommonRequest request);

    @Post(url = "http://127.0.0.1:1234/rpc/v0")
    LotusChainResult getChainHead(@JSONBody RpcRequest request);

    @Post(url = "http://127.0.0.1:1234/rpc/v0")
    String getNonce(@JSONBody CommonRequest request);

    @Post(url = "http://127.0.0.1:1234/rpc/v0")
    String getGasEstimateFeeCap(@JSONBody RpcRequest request);

    @Post(url = "http://127.0.0.1:1234/rpc/v0")
    CommonResult checkAddress(@JSONBody CommonRequest request);

    @Post(url = "http://127.0.0.1:1234/rpc/v0")
    String getGasEstimateGasLimit(@JSONBody ListObjectRequest request);

    @Post(url = "http://127.0.0.1:1234/rpc/v0")
    CommonResult walletSign(@JSONBody CommonRequest request);

    @Post(url = "http://127.0.0.1:1234/rpc/v0",
    headers = {
            "Authorization:Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBbGxvdyI6WyJyZWFkIiwid3JpdGUiLCJzaWduIl19.Px1Y96hPJulVz0DB37U73XkCeDHLnSAf2DiL4Z4Bo4U"
    })
    CommonResult mpoolPushMessage(@JSONBody RpcRequest request);
}
